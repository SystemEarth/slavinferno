% Plot results:

figure(1)
hold on
grid on
xlabel('Pre [kg]')
ylabel('Gas [kg]')
zlabel('Cost-to-go [euro]')
title('Interpolated expected cost-to-go')
surf(P,G,C,'LineStyle','none')
view(2)
colormap

figure(2)
hold on
grid on
xlabel('Pre [kg]')
ylabel('Gas [kg]')
zlabel('Oxygen [kg]')
title('Interpolated oxygen input')
surf(P,G,U1,'LineStyle','none')
view(2)
colormap

figure(3)
hold on
grid on
xlabel('Pre [kg]')
ylabel('Gas [kg]')
zlabel('Carbon [kg]')
title('Interpolated carbon input')
surf(P,G,U2,'LineStyle','none')
view(2)
colormap

figure(4)
hold on
grid on
xlabel('Pre [kg]')
ylabel('Gas [kg]')
zlabel('Ore [kg]')
title('Interpolated ore input')
surf(P,G,U3,'LineStyle','none')
view(2)
colormap

figure(5)
subplot(3,1,1)
hold on
grid on
title('Pre over time')
xlabel('Time [h]')
ylabel('Pre [kg]')
plot(0:1:T,x(1,:))

subplot(3,1,2)
hold on
grid on
title('Gas over time')
xlabel('Time [h]')
ylabel('Gas [kg]')
plot(0:1:T,x(2,:))

subplot(3,1,3)
hold on
grid on
title('Total metal produced over time')
xlabel('Time [h]')
ylabel('Metal [kg]')
plot(0:1:(T-1),cumsum(M))


figure(6)
subplot(3,1,1)
hold on
grid on
title('Oxygen input over time')
xlabel('Time [h]')
ylabel('Oxygen [kg]')
plot(0:1:(T-1),u(1,:))

subplot(3,1,2)
hold on
grid on
title('Carbon input over time')
xlabel('Time [h]')
ylabel('Carbon [kg]')
plot(0:1:(T-1),u(2,:))

subplot(3,1,3)
hold on
grid on
title('Ore input over time')
xlabel('Time [h]')
ylabel('Ore [kg]')
plot(0:1:(T-1),u(3,:))


figure(7)
subplot(2,1,1)
hold on
grid on
title('Uncertainty W_{k+1} over time')
xlabel('Time [h]')
ylabel('W_{k+1}')
plot(0:1:(T-1),W)

subplot(2,1,2)
hold on
grid on
title('Total cost over time')
xlabel('Time [h]')
ylabel('Cost [euro]')
plot(0:1:T,cumsum(c))