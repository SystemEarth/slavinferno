function states = stateOptions(V,dV,rho_P,rho_G)
	% Units of volume of Pre and Gas:
	vP = 0:dV:V;
	vG = 0:dV:V;

	% Pre-allocate for all possible combinations:
	states_vol_raw = zeros(2,numel(vG)*numel(vP));

	% Set up all possilble combinations:
	index = 1;
	for i = 1:numel(vP)
		for j = 1:numel(vG)
			states_vol_raw(:,index) = [vP(i); vG(j)];
			index = index + 1;
		end
	end

	% Only select those which adhere to the volume constraint:
	states_vol = states_vol_raw(:,find(sum(states_vol_raw,1) <= V));

	% Multiply by density to get actual states:
	states = [rho_P; rho_G].*states_vol;
end

