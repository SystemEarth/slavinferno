function x_new = furnace(x,u,w,params)
    % Unpack parameters:
	rho = params.rho;
    k = params.k;
	V = params.V;
	dt = params.dt;
    
    % Unpack states:
    P = x(1);
    G = x(2);
    
    % Unpack inputs:
    O = u(1);
    C = u(2);
    Or = u(3);
    
    % Compute next states:
    P_new = P + dt*rho.P*(k.OrG/(V*rho.Or*rho.G))*w*Or*G - ...
                dt*rho.P*(k.CP/(V*rho.C*rho.P))*w*C*P;
         
    G_new = G + dt*rho.G*(k.CO/(V*rho.O*rho.C))*w*O*C - ...
                dt*rho.G*(k.OrG/(V*rho.Or*rho.G))*w*Or*G + ...
                dt*rho.G*(k.GO/(V*rho.O*rho.G))*w*O*G + ...
                dt*rho.G*(k.CP/(V*rho.P*rho.C))*w*P*C - ...
                k.out*G;
         
    % Pack states:
    x_new = [0; 0];
    
    x_new(1) = P_new;
    x_new(2) = G_new;

end

