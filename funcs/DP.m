function [inputs_opt,J] = DP(states,J_next,Z,t,T,params)
    % Unpack relevant parameters:
    rho= params.rho;  
    V = params.V;
    dV = params.dV;
    
    % Repack parameters for dynamics and costs:
    
    % Pre-allocate for optimal inputs and cost:
    inputs_opt = zeros(3,size(states,2));
    J = zeros(1,size(states,2));
    
    % For each state, repeat:
    for i  = 1:size(states,2)
        
    % Select state:
    state = states(:,i); 
    
    % Set up admissible set of inputs:
    % Map states to volumes:
    state_vol = state./[rho.P; rho.G];
    
    % Compute maximum allowable volume units of input:
    max_units = (V - sum(state_vol))/dV;
    
    % Compute number of possible input combinations:
    n = 0;
    for k = 0:max_units
        for l = 0:k
            for m = 0:l
                n = n + 1;
            end
        end
    end
    
    % Pre-allocate for possible inputs:
    in_vols = zeros(3,n);
    
    % Set up all possilbe input combinations:
    n = 1;
    for k = 0:max_units
        for l = 0:k
            for m = 0:l
                in_vols(:,n) = [max_units - k; ...
                                k - l; ...
                                l - m];

                n = n + 1;
            end
        end
    end
    
    inputs = [rho.O; rho.C; rho.Or].*(dV*in_vols);
    
    % Pre-allocate for expected running cost and expected cost-to-go:
    Eg = zeros(1,size(inputs,2));
    EJ_next = zeros(1,size(inputs,2));
    
    % For each input:
    for k = 1:size(inputs,2)
        % Loop over the uncertainty to get expected values:
        for l = 1:size(Z,2)
            % Update expected running cost:
            Eg(k) = Eg(k) + Z(1,l)*cost(state,inputs(:,k),Z(2,l),t,T,params);

            % Next state with current Z and input:
            next_state = furnace(state,inputs(:,k),Z(2,l),params);

            % Find closest admissible state for which cost-to-go is known:
            NearInd = min(find(min(sum((states-next_state).^2,1)) == sum((states-next_state).^2,1)));

            % Update expected cost to go:
            EJ_next(k) = EJ_next(k) + Z(1,l)*J_next(NearInd);
        end
    end
    
    EJ = Eg + EJ_next;
    
    inputs_opt(:,i) = inputs(:,min(find(min(EJ) == EJ)));
    
    J(i) = min(EJ);
    
    end
    
    fprintf('Done with DP for t =%2.0f \n',t)
    
end

