function [u,J] = get_inputs(x,t,results)
    %load('results.mat')
    
    method = 'cubic';
    
    J = griddata(results.states(1,:),results.states(2,:),results.costs(t+1,:),x(1),x(2),method);
    
    u1 = griddata(results.states(1,:),results.states(2,:),results.inputs(1,:,t+1),x(1),x(2),method);
    u2 = griddata(results.states(1,:),results.states(2,:),results.inputs(2,:,t+1),x(1),x(2),method);
    u3 = griddata(results.states(1,:),results.states(2,:),results.inputs(3,:,t+1),x(1),x(2),method);

    u = max([u1 u2 u3]',0);
    
end

