function c = cost(x,u,w,t,T,params)
    % Unpack parameters:
	rho = params.rho;
    k = params.k;
	p = params.p;
    
    V = params.V;
    dt = params.dt;

    % Unpack states:
    P = x(1);
    G = x(2);
    
    % Unpack inputs:
    O = u(1);
    C = u(2);
    Or = u(3);
    
    % Compute metal:
    M = dt*rho.M*(k.CP/(V*rho.C*rho.P))*w*C*P;
    
    % Compute cost:
    if t < T
        c = p.Or*Or + p.C*C + p.G*k.out*G - p.M*M;
    elseif t == T
        c = p.G*G;
    else
        fprintf('Error: Check time index in cost function \n')
        c = [];
    end
    
end

