clear; close all; clear path; clc;
addpath("./funcs/")
%% Parameters:

load("./data/rho.mat"); % Densities (rho)
load("./data/rates.mat") % Reaction rates and Gas Outflow rate (k)
load("./data/prices.mat"); % Costs (p)

% Furnace volume:
V = 1;
dV = 0.1; % Smallest unit of volume

% Time settings:
dt = 5; % timestep (Reactions only)
T = 10; % Horizon
t = 0:1:T; % Timeline

params = struct("rho",rho,"k",k,"p",p,"V",V,"dV",dV,"dt",dt); % Stack for passing to functions

%% Set up states:
states = stateOptions(V,dV,rho.P,rho.G);

%% Perform dynamic programming:

% Compute terminal costs:
G = zeros(1,size(states,2));
for i = 1:size(states,2)
    G(i) = cost(states(:,i),[0 0 0],0,T,T,params);
end

Z = [0.25, 0.5, 0.25; 0.9,  1,   1.1]; % Uncertainties:

J = zeros(length(t),size(states,2));
J(end,:) = G; % J_T = G

% Perform DP:
inputs_opt = zeros(3,size(states,2),length(t)-1);
for i = 1:(length(t)-1)
    [inputs_opt(:,:,end-(i-1)),J(end-i,:)] = DP(states,J(end-(i-1),:),Z,t(end-i),T,params);
end

% DP results:
results = struct("states", states,"costs",J,"inputs",inputs_opt);

%% Prepare input and cost data at t to plot:
% Time at which to evaluate:
t_test = 5;

% Set up grid:
[P,G] = meshgrid(rho.P*(0:0.025:V),rho.G*(0:0.025:V));

% Pre-allocate for resulting inputs and cost-to-go:
C = zeros(size(P));

% Compute input and cost-to-go for each gridpoint:
for i = 1:numel(P)
   [u,C(i)] = get_inputs([P(i); G(i)],t_test,results);
   %% What happens to this u? Just for plotting heatmaps?
end
%% Run simulation:

x0 = [0; 0]; % Initial state

W = rand(10,1)*0.4 + 0.8; % Noise on Reaction Rate

x = zeros(2,11); % States
x(:,1) = x0;     
u = zeros(3,10); % Inputs
M = zeros(1,10); % Metal Produced
c = zeros(1,11); % Costs

% Simulate furnace:
for i = 1:T
    u(:,i) = get_inputs(x(:,i),i-1,results);
    
    x(:,i+1) = furnace(x(:,i),u(:,i),W(i),params);
	if x(1,i+1)<0
		x(1,i+1) = 0;
	end
    
    M(i) = dt*rho.M*(k.CP/(V*rho.C*rho.P))*W(i)*u(2,i)*x(1,i);
end

% Compute costs:
for i = 1:(length(c)-1)
    c(i) = cost(x(:,i),u(:,i),W(i),i-1,T,params);
end

c(end) = cost(x(:,end),[0; 0; 0],1,T,T,params);

%% plot Results
run('./plotResults.m');