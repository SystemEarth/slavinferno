function x_new = xnew(x,u,w)
%XNEW Summary of this function goes here
%   Detailed explanation goes here

    % Reaction rates:
    k_CO = 0.25;
    k_OrG = 0.2;
    k_GO = 0.05;
    k_CP = 0.9;
    
    % Other parameters:
    V = 10;
    dt = 0.1;
    k_out = 0.5;
    
    % High grade iron ore =~ 65% Hematite, 35% Silicate:
    Vore = 4000; %kg/m^3
    
    % Unpack states:
    P = x(1);
    G = x(2);
    M = x(3);
    
    Solid = P + M;
    Gas = G;
    
    % Unpack inputs:
    O = u(1);
    C = u(2);
    Or = u(3);
    
    
     
    % Compute next states:
    P1 = P + dt*(k_OrG/V)*w*Or*G - ...
             dt*(k_CP/V)*w*C*P;
         
    G1 = G + dt*(k_CO/V)*w*O*C - ...
             dt*(k_OrG/V)*w*Or*G + ...
             dt*(k_GO/V)*w*O*G + ...
             dt*(k_CP/V)*w*P*C - ...
             k_out*G;
         
    M1 = dt*(k_CP/V)*w*C*P;
    
    % Pack states:
    x_new = [0; 0; 0];
    
    x_new(1) = P1;
    x_new(2) = G1;
    x_new(3) = M1;

end

