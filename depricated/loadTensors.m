function loadTensors()
%LOADTENSORS Summary of this function goes here
%   Detailed explanation goes here
    folders = extract(pwd,lettersPattern);
gitlvl = find(folders == "git");
if (length(folders)-gitlvl ~=0)
    gitPath = convertCharsToStrings(repmat('../',1, length(folders)-gitlvl));
else
    gitPath = ".";
end
    addpath(gitPath+"tensor_toolbox/");
    fprintf("Tensor Toolbox added to path ... \n");
end

