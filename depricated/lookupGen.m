function states = lookupGen(ds,bounds)
    %% Set of admissible states:
    
    %% Discretization
    % States (without metal)
    vP =    bounds(1,1):ds:bounds(1,2);
    vG =    bounds(2,1):ds:bounds(2,2);
    % Inputs
    vO =    bounds(3,1):ds:bounds(3,2);
    vC =    bounds(4,1):ds:bounds(4,2);
    vOre =  bounds(5,1):ds:bounds(5,2);
    % Random reaction rate
    vW =    bounds(6,1):ds:bounds(6,2);
    
    %% Table setup
    ncomb = length(vG)*length(vP)*length(vO)*length(vOre)*length(vC)*...
        length(vW);
    states_raw = zeros(ncomb,7);
    index = 1;
    
    %% Table contents
    for i = 1:length(vG)
        for j = 1:length(vP)
            for k = 1:length(vO)
                for l = 1:length(vOre)
                    for m = 1:length(vC)
                        for n = 1:length(vW)
                            states_raw(index,1) = vG(i);
                            states_raw(index,2) = vP(j);
                            states_raw(index,3) = vO(k);
                            states_raw(index,4) = vOre(l);
                            states_raw(index,5) = vC(m);
                            states_raw(index,6) = vW(n);
                            states_raw(index,7) = vG(i) + vP(j)...
                                + vO(k) + vOre(l) + vC(m);

                            index = index + 1;
                        end
                    end
                end
            end
        end
    end

    states = states_raw(find(states_raw(:,7) == 10),:);
end

