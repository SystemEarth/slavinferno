%% Code dump:
% function [u,J] = get_inputs(x,t)
%     load('interpolant.mat')
%     
%     u1 = interpolant(t+1).oxygen(x'); 
%     u2 = interpolant(t+1).carbon(x'); 
%     u3 = interpolant(t+1).ore(x');
%     
%     u = [u1; u2; u3];
%      
%     J = interpolant(t+1).cost(x');
% 
% end

%--------------------------------------------------------------------------

% t_test = 5;
% 
% [P,G] = meshgrid(rho_P*vP,rho_G*vG);
% 
% U1 = zeros(size(P));
% U2 = zeros(size(P));
% U3 = zeros(size(P));
% C = zeros(size(P));
% 
% for i = 1:numel(P)
%    C(i) = griddata(states(1,:),states(2,:),J(t_test+1,:),P(i),G(i),'v4');
%    U1(i) = griddata(states(1,:),states(2,:),inputs_opt(1,:,t_test+1),P(i),G(i),'v4');
%    U2(i) = griddata(states(1,:),states(2,:),inputs_opt(2,:,t_test+1),P(i),G(i),'v4');
%    U3(i) = griddata(states(1,:),states(2,:),inputs_opt(3,:,t_test+1),P(i),G(i),'v4');
% end
% 
% figure(1)
% hold on
% grid on
% surf(P,G,C)
% view(3)
% 
% figure(2)
% hold on
% grid on
% surf(P,G,U1)
% view(3)
% 
% figure(3)
% hold on
% grid on
% surf(P,G,U2)
% view(3)
% 
% figure(4)
% hold on
% grid on
% surf(P,G,U3)
% view(3)

%--------------------------------------------------------------------------

% %% Set up interpolant functions:
% 
% interpolant = struct();
% 
% in_method = 'linear';
% ex_method = 'none';
% 
% for i = 1:T
%     interpolant(i).cost = scatteredInterpolant(states(1,:)',states(2,:)',J(i,:)',in_method,ex_method);
%     interpolant(i).oxygen = scatteredInterpolant(states(1,:)',states(2,:)',inputs_opt(1,:,i)',in_method,ex_method);
%     interpolant(i).carbon = scatteredInterpolant(states(1,:)',states(2,:)',inputs_opt(2,:,i)',in_method,ex_method);
%     interpolant(i).ore = scatteredInterpolant(states(1,:)',states(2,:)',inputs_opt(3,:,i)',in_method,ex_method);
% end
% 
% % Save interpolant:
% save('interpolant.mat','interpolant')