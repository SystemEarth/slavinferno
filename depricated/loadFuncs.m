function loadFuncs()
%LOADFUNCS Summary of this function goes here
%   Detailed explanation goes here
    folders = extract(pwd,lettersPattern);
    funcslvl = find(folders == "git");
    if (length(folders)-funcslvl ~=1)
        funcsPath = convertCharsToStrings(repmat('../',1, length(folders)-funcslvl));
    else
        funcsPath = "./";
    end
        addpath(funcsPath+"funcs/");
        fprintf("Functions added to path... \n");
end

